<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 25.03.2018
  Time: 13:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<html>--%>
  <%--<head>--%>
    <%--<title>$Title$</title>--%>
  <%--</head>--%>
  <%--<body>--%>
  <%--<ul>--%>
    <%--<a href="addemployee.jsp">Add employee</a>--%>
    <%--<a href="addcustomer.jsp">Add customer</a>--%>
    <%--<a href="addgoods">Add goods</a>--%>
    <%--<a href="addorder">Add order</a>--%>
    <%--<a href="addgenre.jsp">Add genre</a>--%>
    <%--<a href="/viewemployees">View employee</a>--%>
    <%--<a href="/viewcustomers">View customer</a>--%>
    <%--<a href="/viewgoods?tgenre=cpu">View cpu</a>--%>
    <%--<a href="/viewgoods?tgenre=videocard">View videocard</a>--%>
    <%--<a href="/viewgoods?tgenre=motherboard">View motherboard</a>--%>
    <%--<a href="/viewgoods?tgenre=hdd">View hdd</a>--%>
    <%--<a href="/viewgoods?tgenre=cooler">View cooler</a>--%>
    <%--<a href="/viewgoods?tgenre=corp">View corp</a>--%>
    <%--<a href="/viewgoods?tgenre=powersupply">View powersupply</a>--%>
    <%--<a href="/viewgoods?tgenre=soundcard">View soundcard</a>--%>
    <%--<a href="/viewgoods?tgenre=networkadapter">View networkadapter</a>--%>
    <%--<a href="/viewgoods?tgenre=opticalstorage">View opticalstorage</a>--%>
    <%--<a href="/viewgoods?tgenre=tvtuner">View tvtuner</a>--%>
    <%--<a href="/viewgoods?tgenre=mouse">View mouse</a>--%>
    <%--<a href="/viewgoods?tgenre=keyboard">View keyboard</a>--%>
    <%--<a href="/viewgoods?tgenre=usbflash">View usbflash</a>--%>
    <%--<a href="/viewgoods?tgenre=memorycard">View memorycard</a>--%>
    <%--<a href="vieworders">View orders</a>--%>
    <%--<a href="viewgenre">View genre</a>--%>
  <%--</ul>--%>
  <%--</body>--%>
<%--</html>--%>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Computer Electronic Store| Home</title>
  <!-- for-mobile-apps -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="Electronic Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	SmartPhone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
  function hideURLbar(){ window.scrollTo(0,1); } </script>
  <!-- //for-mobile-apps -->
  <!-- Custom Theme files -->
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/fasthover.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
  <!-- //Custom Theme files -->
  <!-- font-awesome icons -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- //font-awesome icons -->
  <!-- js -->
  <script src="js/jquery.min.js"></script>
  <link rel="stylesheet" href="css/jquery.countdown.css" /> <!-- countdown -->
  <!-- //js -->
  <!-- web fonts -->
  <link href='//fonts.googleapis.com/css?family=Glegoo:400,700' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
  <!-- //web fonts -->
  <!-- start-smooth-scrolling -->
  <script type="text/javascript">
      jQuery(document).ready(function($) {
          $(".scroll").click(function(event){
              event.preventDefault();
              $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
          });
      });
  </script>
  <!-- //end-smooth-scrolling -->
</head>
<body>
<!-- for bootstrap working -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- //for bootstrap working -->
<!-- header -->
<div class="header" id="home1">
  <div class="container">
    <div class="w3l_logo">
      <h1><a href="index.jsp">Computer Electronic Store<span>Good deal, it's real.</span></a></h1>
    </div>

  </div>
</div>
<!-- //header -->
<!-- navigation -->
<div class="navigation">
  <div class="container">
    <nav class="navbar navbar-default">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header nav_2">
        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav">
          <li><a href="index.jsp" class="act">Home</a></li>
          <!-- Mega Menu -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
            <ul class="dropdown-menu multi-column columns-3">
              <div class="row">
                <div class="col-sm-3">
                  <ul class="multi-column-dropdown">
                    <h6>Computer parts</h6>
                    <%--<li><a href="products.html">Mobile Phones</a></li>--%>
                    <%--<li><a href="products.html">Mp3 Players <span>New</span></a></li>--%>
                    <%--<li><a href="products.html">Popular Models</a></li>--%>
                    <%--<li><a href="products.html">All Tablets<span>New</span></a></li>--%>
                    <li><a href="/viewgoods?tgenre=cpu">Cpu <span>New</span></a></li>
                    <li><a href="/viewgoods?tgenre=videocard">Videocard<span>New</span></a></li>
                    <li><a href="/viewgoods?tgenre=motherboard">Motherboard<span>New</span></a></li>
                    <li><a href="/viewgoods?tgenre=hdd">Hdd<span>New</span></a></li>
                    <li><a href="/viewgoods?tgenre=cooler">Cooler<span>New</span></a></li>
                    <li><a href="/viewgoods?tgenre=corp">Corp<span>New</span></a></li>
                    <li><a href="/viewgoods?tgenre=powersupply">Powersupply <span>New</span></a></li>
                    <li><a href="/viewgoods?tgenre=soundcard">Soundcard <span>New</span></a></li>
                  </ul>
                </div>
                <div class="col-sm-3">
                  <ul class="multi-column-dropdown">
                    <h6>Accessories</h6>
                    <li><a href="/viewgoods?tgenre=networkadapter">Network adapter</a></li>
                    <li><a href="/viewgoods?tgenre=tvtuner">TV-tuner</a></li>
                    <li><a href="/viewgoods?tgenre=mouse">Mouse</a></li>
                    <li><a href="/viewgoods?tgenre=keyboard">Keyboard</a></li>
                    <li><a href="/viewgoods?tgenre=usbflash">Usb-flash</a></li>
                    <li><a href="/viewgoods?tgenre=memorycard">Memory card</a></li>
                  </ul>
                </div>

                <div class="clearfix"></div>
              </div>
            </ul>
          </li>
          <li><a href="/viewemployees">Employee</a></li>
          <li><a href="/viewcustomers">Customer</a></li>
          <li><a href="vieworders">Orders</a></li>
          <li><a href="viewgenre">Genre</a></li>
        </ul>
      </div>
    </nav>
  </div>
</div>
<!-- //navigation -->
<!-- banner -->
<div class="banner">
  <div class="container">
    <h3>Computer Electronic<span>Store</span></h3>
  </div>
</div>
<!-- //banner -->
<!-- banner-bottom -->
<div class="banner-bottom">
  <div class="container">
    <div class="col-md-7 wthree_banner_bottom_right">
      <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home">Add employee</a></li>
          <li role="presentation"><a href="#audio" role="tab" id="audio-tab" data-toggle="tab" aria-controls="audio">Add customer</a></li>
          <li role="presentation"><a href="#video" role="tab" id="video-tab" data-toggle="tab" aria-controls="video">Add genre</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
          <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
            <div class="agile_ecommerce_tabs">
                <div class="review_grids">
                  <h5>Add A Employee</h5>
                  <form action="/addemployee" method="post">
                    <h4><label for="name">Employee name:</label></h4>
                    <input type="text" name="name"  id="name" required></br>
                    <h4><label for="age">Age:</label></h4>
                    <input type="text" name="age"  id="age" required></br>
                    <input type="submit" value="Submit" >
                      <input type="reset" value="Reset">
                  </form>
                </div>
              <div class="clearfix"> </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="audio" aria-labelledby="audio-tab">
            <div class="agile_ecommerce_tabs">
              <div class="review_grids">
                <h5>Add A Customer</h5>
                <form action="/addcustomer" method="post">
                  <h4><label for="name">Customer name:</label></h4>
                  <input type="text" name="name"  id="customernamename" required></br>
                  <h4><label for="age">Age:</label></h4>
                  <input type="text" name="age"  id="customerage" required></br>
                  <input type="submit" value="Submit" >
                    <input type="reset" value="Reset">
                </form>
              </div>
              <div class="clearfix"> </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="video" aria-labelledby="video-tab">
            <div class="agile_ecommerce_tabs">
              <div class="review_grids">
                <h5>Add Genre</h5>
                  <form action="/addgenre" method="post">
                        <h4><label for="name">Name:</label></h4>
                        <input type="text" name="name" id="namegenre" required></br>
                        <h4><label for="code">Code:</label></h4>
                        <input type="text" name="code" id="code" required></br>
                        <input type="submit">
                        <input type="reset" value="Reset">
                  </form>
              </div>
              <div class="clearfix"> </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>
<!-- //banner-bottom -->
<!-- modal-video -->
<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <section>
        <div class="modal-body">
          <div class="col-md-5 modal_body_left">
            <img src="images/3.jpg" alt=" " class="img-responsive" />
          </div>
          <div class="col-md-7 modal_body_right">
            <h4>The Best Mobile Phone 3GB</h4>
            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
              commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            <div class="rating">
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="modal_body_right_cart simpleCart_shelfItem">
              <p><span>$380</span> <i class="item_price">$350</i></p>
              <form action="#" method="post">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="add" value="1">
                <input type="hidden" name="w3ls_item" value="Mobile Phone1">
                <input type="hidden" name="amount" value="350.00">
                <button type="submit" class="w3ls-cart">Add to cart</button>
              </form>
            </div>
            <h5>Color</h5>
            <div class="color-quality">
              <ul>
                <li><a href="#"><span></span></a></li>
                <li><a href="#" class="brown"><span></span></a></li>
                <li><a href="#" class="purple"><span></span></a></li>
                <li><a href="#" class="gray"><span></span></a></li>
              </ul>
            </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </section>
    </div>
  </div>
</div>
<div class="modal video-modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModal1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <section>
        <div class="modal-body">
          <div class="col-md-5 modal_body_left">
            <img src="images/9.jpg" alt=" " class="img-responsive" />
          </div>
          <div class="col-md-7 modal_body_right">
            <h4>Multimedia Home Accessories</h4>
            <p>Ut enim ad minim veniam, quis nostrud
              exercitation ullamco laboris nisi ut aliquip ex ea
              commodo consequat.Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore
              eu fugiat nulla pariatur. Excepteur sint occaecat
              cupidatat non proident, sunt in culpa qui officia
              deserunt mollit anim id est laborum.</p>
            <div class="rating">
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="modal_body_right_cart simpleCart_shelfItem">
              <p><span>$180</span> <i class="item_price">$150</i></p>
              <form action="#" method="post">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="add" value="1">
                <input type="hidden" name="w3ls_item" value="Headphones">
                <input type="hidden" name="amount" value="150.00">
                <button type="submit" class="w3ls-cart">Add to cart</button>
              </form>
            </div>
            <h5>Color</h5>
            <div class="color-quality">
              <ul>
                <li><a href="#"><span></span></a></li>
                <li><a href="#" class="brown"><span></span></a></li>
                <li><a href="#" class="purple"><span></span></a></li>
                <li><a href="#" class="gray"><span></span></a></li>
              </ul>
            </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </section>
    </div>
  </div>
</div>
<div class="modal video-modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModal2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <section>
        <div class="modal-body">
          <div class="col-md-5 modal_body_left">
            <img src="images/11.jpg" alt=" " class="img-responsive" />
          </div>
          <div class="col-md-7 modal_body_right">
            <h4>Quad Core Colorful Laptop</h4>
            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia  deserunt.</p>
            <div class="rating">
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="modal_body_right_cart simpleCart_shelfItem">
              <p><span>$880</span> <i class="item_price">$850</i></p>
              <form action="#" method="post">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="add" value="1">
                <input type="hidden" name="w3ls_item" value="Laptop">
                <input type="hidden" name="amount" value="850.00">
                <button type="submit" class="w3ls-cart">Add to cart</button>
              </form>
            </div>
            <h5>Color</h5>
            <div class="color-quality">
              <ul>
                <li><a href="#"><span></span></a></li>
                <li><a href="#" class="brown"><span></span></a></li>
                <li><a href="#" class="purple"><span></span></a></li>
                <li><a href="#" class="gray"><span></span></a></li>
              </ul>
            </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </section>
    </div>
  </div>
</div>
<div class="modal video-modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModal3">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <section>
        <div class="modal-body">
          <div class="col-md-5 modal_body_left">
            <img src="images/14.jpg" alt=" " class="img-responsive" />
          </div>
          <div class="col-md-7 modal_body_right">
            <h4>Cool Single Door Refrigerator </h4>
            <p>Duis aute irure dolor inreprehenderit in voluptate velit esse cillum dolore
              eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <div class="rating">
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="modal_body_right_cart simpleCart_shelfItem">
              <p><span>$950</span> <i class="item_price">$820</i></p>
              <form action="#" method="post">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="add" value="1">
                <input type="hidden" name="w3ls_item" value="Mobile Phone1">
                <input type="hidden" name="amount" value="820.00">
                <button type="submit" class="w3ls-cart">Add to cart</button>
              </form>
            </div>
            <h5>Color</h5>
            <div class="color-quality">
              <ul>
                <li><a href="#"><span></span></a></li>
                <li><a href="#" class="brown"><span></span></a></li>
                <li><a href="#" class="purple"><span></span></a></li>
                <li><a href="#" class="gray"><span></span></a></li>
              </ul>
            </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </section>
    </div>
  </div>
</div>
<div class="modal video-modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModal4">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <section>
        <div class="modal-body">
          <div class="col-md-5 modal_body_left">
            <img src="images/17.jpg" alt=" " class="img-responsive" />
          </div>
          <div class="col-md-7 modal_body_right">
            <h4>New Model Mixer Grinder</h4>
            <p>Excepteur sint occaecat laboris nisi ut aliquip ex ea
              commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
              eu fugiat nulla pariatur cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <div class="rating">
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star-.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="rating-left">
                <img src="images/star.png" alt=" " class="img-responsive" />
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="modal_body_right_cart simpleCart_shelfItem">
              <p><span>$460</span> <i class="item_price">$450</i></p>
              <form action="#" method="post">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="add" value="1">
                <input type="hidden" name="w3ls_item" value="Mobile Phone1">
                <input type="hidden" name="amount" value="450.00">
                <button type="submit" class="w3ls-cart">Add to cart</button>
              </form>
            </div>
            <h5>Color</h5>
            <div class="color-quality">
              <ul>
                <li><a href="#"><span></span></a></li>
                <li><a href="#" class="brown"><span></span></a></li>
                <li><a href="#" class="purple"><span></span></a></li>
                <li><a href="#" class="gray"><span></span></a></li>
              </ul>
            </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </section>
    </div>
  </div>
</div>
<!-- footer -->
<div class="footer">
  <div class="footer-copy">
    <div class="footer-copy1">
      <div class="footer-copy-pos">
        <a href="#home1" class="scroll"><img src="images/arrow.png" alt=" " class="img-responsive" /></a>
      </div>
    </div>
    <div class="container">
      <p>&copy; 2018 Computer Electronic Store. All rights reserved</p>
    </div>
  </div>
</div>
<!-- //footer -->
</body>
</html>