<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 25.03.2018
  Time: 14:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<html>--%>
<%--<head>--%>
    <%--<title>View Customer</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--<table border="1">--%>
    <%--<tr>--%>
        <%--<td>Номер</td>--%>
        <%--<td>Имя покупателя</td>--%>
        <%--<td>Возраст покупателя</td>--%>
        <%--<td>Действие</td>--%>
    <%--</tr>--%>
    <%--<c:forEach items="${requestScope.viewcustomers}" var="customer">--%>
    <%--<tr>--%>
        <%--<td>${customer.id}</td>--%>
        <%--<td><c:out value="${customer.name}"/></td>--%>
        <%--<td><c:out value="${customer.age}"/></td>--%>
        <%--<td>--%>
            <%--<a href="/deletecustomer?id=${customer.id}">Удалить</a>--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--</c:forEach>--%>
<%--</table>--%>
<%--<a href="/addcustomer.jsp">Add new customer</a>--%>
<%--</body>--%>
<%--</html>--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Computer Electronic Store| View customer</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Electronic Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	SmartPhone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="js/jquery.min.js"></script>
    <!-- //js -->
    <!-- web fonts -->
    <link href='//fonts.googleapis.com/css?family=Glegoo:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //web fonts -->
    <!-- for bootstrap working -->
    <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
    <!-- //for bootstrap working -->
    <!-- start-smooth-scrolling -->
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
</head>
<body>
<!-- header -->
<div class="header" id="home1">
    <div class="container">
        <div class="w3l_logo">
            <h1><a href="index.jsp">Computer Electronic Store<span>Good deal, it's real.</span></a></h1>
        </div>

    </div>
</div>
<!-- //header -->
<div class="navigation">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header nav_2">
                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp" class="act">Home</a></li>
                    <!-- Mega Menu -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="col-sm-3">
                                    <ul class="multi-column-dropdown">
                                        <h6>Computer parts</h6>
                                        <li><a href="/viewgoods?tgenre=cpu">Cpu <span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=videocard">Videocard<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=motherboard">Motherboard<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=hdd">Hdd<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=cooler">Cooler<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=corp">Corp<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=powersupply">Powersupply <span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=soundcard">Soundcard <span>New</span></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-3">
                                    <ul class="multi-column-dropdown">
                                        <h6>Accessories</h6>
                                        <li><a href="/viewgoods?tgenre=networkadapter">Network adapter</a></li>
                                        <li><a href="/viewgoods?tgenre=tvtuner">TV-tuner</a></li>
                                        <li><a href="/viewgoods?tgenre=mouse">Mouse</a></li>
                                        <li><a href="/viewgoods?tgenre=keyboard">Keyboard</a></li>
                                        <li><a href="/viewgoods?tgenre=usbflash">Usb-flash</a></li>
                                        <li><a href="/viewgoods?tgenre=memorycard">Memory card</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </ul>
                    </li>
                    <li><a href="/viewemployees">Employee</a></li>
                    <li><a href="/viewcustomers">Customer</a></li>
                    <li><a href="vieworders">Orders</a></li>
                    <li><a href="viewgenre">Genre</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- //navigation -->
<!-- banner -->
<div class="banner banner10">
    <div class="container">
        <h2>View customer</h2>
    </div>
</div>
<!-- //banner -->
<!-- breadcrumbs -->
<div class="breadcrumb_dress">
    <div class="container">
        <ul>
            <li><a href="index.jsp"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a> <i>/</i></li>
            <li>View customer</li>
        </ul>
    </div>
</div>
<!-- //breadcrumbs -->
<!-- faq -->
<div class="faq">
    <div class="container">
        <h3 class="agileits-title">View customer</h3>
        <div class="review_grids">
            <h3 class="w3ls-hdg">Viewing a customers</h3>
            <table class="table" border="1">
                <tr>
                    <td><h5 id="h5.-bootstrap-heading">Номер</h5></td>
                    <td><h5 id="h5.-bootstrap-heading">Имя покупателя</h5></td>
                    <td><h5 id="h5.-bootstrap-heading">Возраст покупателя</h5></td>
                    <td><h5 id="h5.-bootstrap-heading">Действие</h5></td>
                </tr>
                <c:forEach items="${requestScope.viewcustomers}" var="customer">
                    <tr>
                        <td>${customer.id}</td>
                        <td><c:out value="${customer.name}"/></td>
                        <td><c:out value="${customer.age}"/></td>
                        <td>
                            <a href="/deletecustomer?id=${customer.id}">Удалить</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<!-- //faq -->
<!-- footer -->
<div class="footer">
    <div class="footer-copy">
        <div class="footer-copy1">
            <div class="footer-copy-pos">
                <a href="#home1" class="scroll"><img src="images/arrow.png" alt=" " class="img-responsive" /></a>
            </div>
        </div>
        <div class="container">
            <p>&copy; 2018 Computer Electronic Store. All rights reserved</p>
        </div>
    </div>
</div>
<!-- //footer -->

</body>
</html>
