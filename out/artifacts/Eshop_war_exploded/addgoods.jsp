<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 26.03.2018
  Time: 22:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<html>--%>
<%--<head>--%>
    <%--<title>Add goods</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--<table>--%>
    <%--<form action="/addgoods" method="post">--%>
        <%--<tr>--%>
            <%--<td>--%>
                <%--<label for="tgenre">Genre:</label>--%>
                <%--<select  name="tgenre" id="tgenre" required>--%>
                        <%--&lt;%&ndash;<option value="cpu">Cpu</option>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;<option value="videocard">Videocard</option>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;<option value="motherboard">Motherboard</option>&ndash;%&gt;--%>
                            <%--<c:forEach items="${requestScope.tgenre}" var="tgenre">--%>
                                <%--<option value="${tgenre.code}">${tgenre.name}</option>--%>
                            <%--</c:forEach>--%>
                <%--</select>--%>
            <%--</td>--%>
        <%--</tr>--%>
        <%--<tr>--%>
            <%--<td>--%>
                <%--<label for="title">Title:</label>--%>
                <%--<input type="text" name="title" id="title" required>--%>
            <%--</td>--%>
        <%--</tr>--%>
        <%--&lt;%&ndash;<tr>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<td>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<label for="name">Password:</label>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<input type="password" name="password" id="password" required>&ndash;%&gt;--%>
        <%--&lt;%&ndash;</td>&ndash;%&gt;--%>

        <%--&lt;%&ndash;</tr>&ndash;%&gt;--%>
        <%--<tr>--%>
            <%--<td>--%>
                <%--<label for="vendor">Vendor:</label>--%>
                <%--<input type="text" name="vendor" id="vendor" required>--%>
            <%--</td>--%>
        <%--</tr>--%>
        <%--<tr>--%>
            <%--<td>--%>
                <%--<label for="price">Price:</label>--%>
                <%--<input type="text" name="price" id="price" required>--%>
            <%--</td>--%>
        <%--</tr>--%>
        <%--&lt;%&ndash;<tr>&ndash;%&gt;--%>
            <%--&lt;%&ndash;<td>&ndash;%&gt;--%>
                <%--&lt;%&ndash;<label for="genre">Genre:</label>&ndash;%&gt;--%>
                <%--&lt;%&ndash;<input type="text" name="genre" id="genre" required>&ndash;%&gt;--%>
            <%--&lt;%&ndash;</td>&ndash;%&gt;--%>
        <%--&lt;%&ndash;</tr>&ndash;%&gt;--%>
        <%--<tr>--%>
            <%--<td>--%>
                <%--<input type="submit">--%>
            <%--</td>--%>
            <%--<td>--%>
                <%--<input type="reset" value="Reset">--%>
            <%--</td>--%>
        <%--</tr>--%>
    <%--</form>--%>
<%--</table>--%>
<%--</body>--%>
<%--</html>--%>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Computer Electronic Store| Add goods</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Electronic Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	SmartPhone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="js/jquery.min.js"></script>
    <!-- //js -->
    <!-- web fonts -->
    <link href='//fonts.googleapis.com/css?family=Glegoo:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //web fonts -->
    <!-- for bootstrap working -->
    <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
    <!-- //for bootstrap working -->
    <!-- start-smooth-scrolling -->
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
</head>
<body>
<!-- header -->
<div class="header" id="home1">
    <div class="container">
        <div class="w3l_logo">
            <h1><a href="index.jsp">Computer Electronic Store<span>Good deal, it's real.</span></a></h1>
        </div>

    </div>
</div>
<!-- //header -->
<div class="navigation">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header nav_2">
                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp" class="act">Home</a></li>
                    <!-- Mega Menu -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="col-sm-3">
                                    <ul class="multi-column-dropdown">
                                        <h6>Computer parts</h6>
                                        <li><a href="/viewgoods?tgenre=cpu">Cpu <span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=videocard">Videocard<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=motherboard">Motherboard<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=hdd">Hdd<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=cooler">Cooler<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=corp">Corp<span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=powersupply">Powersupply <span>New</span></a></li>
                                        <li><a href="/viewgoods?tgenre=soundcard">Soundcard <span>New</span></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-3">
                                    <ul class="multi-column-dropdown">
                                        <h6>Accessories</h6>
                                        <li><a href="/viewgoods?tgenre=networkadapter">Network adapter</a></li>
                                        <li><a href="/viewgoods?tgenre=tvtuner">TV-tuner</a></li>
                                        <li><a href="/viewgoods?tgenre=mouse">Mouse</a></li>
                                        <li><a href="/viewgoods?tgenre=keyboard">Keyboard</a></li>
                                        <li><a href="/viewgoods?tgenre=usbflash">Usb-flash</a></li>
                                        <li><a href="/viewgoods?tgenre=memorycard">Memory card</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </ul>
                    </li>
                    <li><a href="/viewemployees">Employee</a></li>
                    <li><a href="/viewcustomers">Customer</a></li>
                    <li><a href="vieworders">Orders</a></li>
                    <li><a href="viewgenre">Genre</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- //navigation -->
<!-- banner -->
<div class="banner banner10">
    <div class="container">
        <h2>Add goods</h2>
    </div>
</div>
<!-- //banner -->
<!-- breadcrumbs -->
<div class="breadcrumb_dress">
    <div class="container">
        <ul>
            <li><a href="index.jsp"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a> <i>/</i></li>
            <li>Add goods</li>
        </ul>
    </div>
</div>
<!-- //breadcrumbs -->
<!-- faq -->
<div class="faq">
    <div class="container">
        <h3 class="agileits-title">Add goods</h3>
        <div class="review_grids">
            <h3 class="w3ls-hdg">Adding a new goods</h3>
            <div class="tab-content">
                <div class="tab-pane active" id="horizontal-form">
                    <form class="form-horizontal" action="/addgoods" method="post">
                        <div class="form-group">
                            <label for="tgenre" class="col-sm-2 control-label">Genre</label>
                            <div class="col-sm-8">
                                <select name="tgenre" id="tgenre" class="form-control1">
                                    <c:forEach items="${requestScope.tgenre}" var="tgenre">
                                        <option value="${tgenre.code}">${tgenre.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control1" name="title" id="title" placeholder="Title">
                            </div>
                            <div class="col-sm-2 jlkdfj1">
                                <p class="help-block">Title of goods</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="vendor" class="col-sm-2 control-label">Vendor</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control1" name="vendor" id="vendor" placeholder="Vendor">
                            </div>
                            <div class="col-sm-2 jlkdfj1">
                                <p class="help-block">Vendor of goods</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control1" name="price" id="price" placeholder="Price">
                            </div>
                            <div class="col-sm-2 jlkdfj1">
                                <p class="help-block">Price of goods</p>
                            </div>
                        </div>
                        <input type="submit" value="Submit" >
                        <input type="reset" value="Reset">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //faq -->
<!-- footer -->
<div class="footer">
    <div class="footer-copy">
        <div class="footer-copy1">
            <div class="footer-copy-pos">
                <a href="#home1" class="scroll"><img src="images/arrow.png" alt=" " class="img-responsive" /></a>
            </div>
        </div>
        <div class="container">
            <p>&copy; 2018 Computer Electronic Store. All rights reserved</p>
        </div>
    </div>
</div>
<!-- //footer -->

</body>
</html>
