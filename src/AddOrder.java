
import by.nosenko.eshop.model.DAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "AddOrder", urlPatterns = "/addorder")
public class AddOrder extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String customer = request.getParameter("customer");
        String employee = request.getParameter("employee");
        String cpu = request.getParameter("cpu");
        String videocard = request.getParameter("videocard");
        String motherboard = request.getParameter("motherboard");
        String hdd = request.getParameter("hdd");
        String ram = request.getParameter("ram");
        String cooler = request.getParameter("cooler");
        String corp = request.getParameter("corp");
        String powersupply = request.getParameter("powersupply");
        String soundcard = request.getParameter("soundcard");
        String networkadapter = request.getParameter("networkadapter");
        String opticalstorage = request.getParameter("opticalstorage");
        String tvtuner = request.getParameter("tvtuner");
        String mouse = request.getParameter("mouse");
        String keyboard = request.getParameter("keyboard");
        String usbflash = request.getParameter("usbflash");
        String memorycard = request.getParameter("memorycard");
        String goods = ""+cpu+", "+videocard+", "+motherboard+", "+hdd+", "+ram+", "+cooler+", "+corp+", "+powersupply+", "+soundcard+", "+networkadapter+", "+opticalstorage+", "+tvtuner+", "+mouse+", "+keyboard+", "+usbflash+", "+memorycard;

        try {
            DAO.addOrder(customer, employee, goods);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/vieworders");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setAttribute("genres", DAO.getGenre());
            request.setAttribute("customers", DAO.getCustomers());
            request.setAttribute("employees", DAO.getEmployees());
            request.setAttribute("cpu", DAO.getGoods("cpu"));
            request.setAttribute("videocard", DAO.getGoods("videocard"));
            request.setAttribute("motherboard", DAO.getGoods("motherboard"));
            request.setAttribute("hdd", DAO.getGoods("hdd"));
            request.setAttribute("ram", DAO.getGoods("ram"));
            request.setAttribute("cooler", DAO.getGoods("cooler"));
            request.setAttribute("corp", DAO.getGoods("corp"));
            request.setAttribute("powersupply", DAO.getGoods("powersupply"));
            request.setAttribute("soundcard", DAO.getGoods("soundcard"));
            request.setAttribute("networkadapter", DAO.getGoods("networkadapter"));
            request.setAttribute("opticalstorage", DAO.getGoods("opticalstorage"));
            request.setAttribute("tvtuner", DAO.getGoods("tvtuner"));
            request.setAttribute("mouse", DAO.getGoods("mouse"));
            request.setAttribute("keyboard", DAO.getGoods("keyboard"));
            request.setAttribute("usbflash", DAO.getGoods("usbflash"));
            request.setAttribute("memorycard", DAO.getGoods("memorycard"));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("/addorder.jsp").forward(request,response);
    }
}
