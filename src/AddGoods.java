
import by.nosenko.eshop.model.DAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "AddGoods", urlPatterns = "/addgoods")
public class AddGoods extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String vendor = request.getParameter("vendor");
        Double price = Double.parseDouble(request.getParameter("price"));
        String tgenre = request.getParameter("tgenre");
        try {
            DAO.addGoods(title, vendor, price, tgenre);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/viewgoods?tgenre="+tgenre+"");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            request.setAttribute("tgenre", DAO.getGenre());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("/addgoods.jsp").forward(request,response);
    }
}
