package by.nosenko.eshop.model;

public class Genre {
    private long id; //уникальный номер книги
    private String name; //название книги
    private String code; //автор книги

    public Genre(long id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString(){
        return "Genre{" +
                "id" + getId() +
                ", name='" + getName() +
                ", code='" + getCode() +
                '\'' +
                '}';
    }
}
