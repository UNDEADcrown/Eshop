package by.nosenko.eshop.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAO {
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/eshop", "root", "");
    }
//    static List<Post> posts;
//    static {
//        posts = new ArrayList<Post>();
//        posts.add(new Post(1,"Hello 1!"));
//        posts.add(new Post(2,"Hello 2!"));
//        posts.add(new Post(3,"Hello 3!"));
//    }

    public static List<Employee> getEmployees() throws SQLException, ClassNotFoundException {
//        return posts;
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("SELECT id, name, age from employees");

             ResultSet resultSet = ps.executeQuery();) {


            ArrayList<Employee> employees = new ArrayList<>();
            while (resultSet.next()) {

                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                int age = resultSet.getInt(3);
                employees.add(new Employee(id, name, age));

            }
            return employees;
        }
    }

    public static List<Customer> getCustomers() throws SQLException, ClassNotFoundException {
//        return posts;
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("SELECT id, name, age from customers");

             ResultSet resultSet = ps.executeQuery();) {


            ArrayList<Customer> customers = new ArrayList<>();
            while (resultSet.next()) {

                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                int age = resultSet.getInt(3);
                customers.add(new Customer(id, name, age));

            }
            return customers;
        }
    }

    public static List<Goods> getGoods(String tgenre) throws SQLException, ClassNotFoundException {
//        return posts;
        if (tgenre.equals("cpu")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from cpu");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        } else if (tgenre.equals("videocard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from videocard");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        } else if (tgenre.equals("motherboard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("hdd")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from hdd");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("ram")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from ram");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("cooler")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from cooler");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("corp")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from corp");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("powersupply")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from powersupply");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("soundcard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from soundcard");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("networkadapter")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from networkadapter");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("opticalstorage")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from opticalstorage");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("tvtuner")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from tvtuner");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("mouse")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from mouse");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("keyboard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from keyboard");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("usbflash")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from usbflash");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else if (tgenre.equals("memorycard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from memorycard");
                 //PreparedStatement ps = c.prepareStatement("SELECT id, title, vendor, price, genre from motherboard");

                 ResultSet resultSet = ps.executeQuery();) {


                ArrayList<Goods> goods = new ArrayList<>();
                while (resultSet.next()) {

                    int id = resultSet.getInt(1);
                    String title = resultSet.getString(2);
                    String vendor = resultSet.getString(3);
                    Double price = resultSet.getDouble(4);
                    String genre = resultSet.getString(5);
                    goods.add(new Goods(id, title, vendor, price, genre));

                }
                return goods;
            }
        }
        else {
            System.out.println("I can't find yours table of goods");
        }
        return null;
    }

    public static List<Genre> getGenre() throws SQLException, ClassNotFoundException {
//        return posts;
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("SELECT id, name, code from genre");

             ResultSet resultSet = ps.executeQuery();) {


            ArrayList<Genre> genre = new ArrayList<>();
            while (resultSet.next()) {

                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String code = resultSet.getString(3);
                genre.add(new Genre(id, name, code));

            }
            return genre;
        }
    }

    public static List<Order> getOrder() throws SQLException, ClassNotFoundException {
//        return posts;
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("SELECT id, customername, employeename, goods from orders");

             ResultSet resultSet = ps.executeQuery();) {


            ArrayList<Order> orders = new ArrayList<>();
            while (resultSet.next()) {

                int id = resultSet.getInt(1);
                String customername = resultSet.getString(2);
                String employeename = resultSet.getString(3);
                String goods = resultSet.getString(4);
                orders.add(new Order(id, customername, employeename, goods));

            }
            return orders;
        }
    }


    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        System.out.println(getEmployees());
        System.out.println(getCustomers());
        DAO.addGoods("Test", "Vendor", 100, "cpu");
        System.out.println(getGoods("cpu"));
        System.out.println(getGoods("videocard"));
        System.out.println(getGoods("motherboard"));
        System.out.println(getGenre());
        System.out.println(getOrder());


    }

    public static void deleteEmployee(int id) throws SQLException, ClassNotFoundException {
//        Post d = null;
//        for (Post p: posts) {
//            if(p.id == id) {
//               d=p;
//            }
//        }
//        if(d!=null) {
//            posts.remove(d);
//        }

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("DELETE FROM employees WHERE id=?");
        ) {
            ps.setInt(1, id);
            ps.executeUpdate();

        }
    }

    public static void deleteCustomer(int id) throws SQLException, ClassNotFoundException {

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("DELETE FROM customers WHERE id=?");
        ) {
            ps.setInt(1, id);
            ps.executeUpdate();

        }
    }

    public static void deleteGoods(int id, String tgenre) throws SQLException, ClassNotFoundException {

        if (tgenre.equals("cpu")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM cpu WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        } else if (tgenre.equals("videocard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM videocard WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        } else if (tgenre.equals("motherboard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM motherboard WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("hdd")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM hdd WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("ram")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM ram WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("cooler")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM cooler WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("corp")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM corp WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("powersupply")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM powersupply WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("soundcard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM soundcard WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("networkadapter")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM networkadapter WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("opticalstorage")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM opticalstorage WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("tvtuner")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM tvtuner WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("mouse")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM mouse WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("keyboard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM keyboard WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("usbflash")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM usbflash WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else if (tgenre.equals("memorycard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("DELETE FROM memorycard WHERE id=?");
            ) {
                ps.setInt(1, id);
                ps.executeUpdate();

            }
        }
        else {
            System.out.println("I can't find yours table of goods");
        }
    }

    public static void deleteOrder(int id) throws SQLException, ClassNotFoundException {

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("DELETE FROM orders WHERE id=?");
        ) {
            ps.setInt(1, id);
            ps.executeUpdate();

        }
    }

    public static void deleteGenre(int id) throws SQLException, ClassNotFoundException {

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("DELETE FROM genre WHERE id=?");
        ) {
            ps.setInt(1, id);
            ps.executeUpdate();

        }
    }

    public static void addEmployee(String name, int age) throws SQLException, ClassNotFoundException {
//        posts.add(new Post(posts.size(), txt));

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("INSERT INTO employees (name, age) VALUES (?,?)");
        ) {
            ps.setString(1, name);
            ps.setInt(2, age);
            ps.executeUpdate();

        }
    }

    public static void addCustomer(String name, int age) throws SQLException, ClassNotFoundException {
//        posts.add(new Post(posts.size(), txt));

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("INSERT INTO customers (name, age) VALUES (?,?)");
        ) {
            ps.setString(1, name);
            ps.setInt(2, age);
            ps.executeUpdate();

        }
    }

    public static void addGoods(String title, String vendor, double price, String genre) throws SQLException, ClassNotFoundException {
//        posts.add(new Post(posts.size(), txt));

        if (genre.equals("cpu")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO cpu (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        } else if (genre.equals("motherboard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO motherboard (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        } else if (genre.equals("videocard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO videocard (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("hdd")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO hdd (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("ram")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO ram (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("cooler")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO cooler (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("corp")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO corp (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("powersupply")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO powersupply (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("soundcard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO soundcard (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("networkadapter")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO networkadapter (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("opticalstorage")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO opticalstorage (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("tvtuner")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO tvtuner (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("mouse")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO mouse (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("keyboard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO keyboard (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("usbflash")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO usbflash (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else if (genre.equals("memorycard")) {
            try (Connection c = getConnection();
                 PreparedStatement ps = c.prepareStatement("INSERT INTO memorycard (title, vendor, price, genre) VALUES (?,?,?,?)");
            ) {
                ps.setString(1, title);
                ps.setString(2, vendor);
                ps.setDouble(3, price);
                ps.setString(4, genre);
                ps.executeUpdate();

            }
        }
        else {
            System.out.println("I can't find yours table");
        }

    }

    public static void addOrder(String customername, String employeename, String goods) throws SQLException, ClassNotFoundException {
//        posts.add(new Post(posts.size(), txt));

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("INSERT INTO orders (customername, employeename, goods) VALUES (?,?,?)");
        ) {
            ps.setString(1, customername);
            ps.setString(2, employeename);
            ps.setString(3, goods);
            ps.executeUpdate();

        }
    }

    public static void addGenre(String name, String code) throws SQLException, ClassNotFoundException {
//        posts.add(new Post(posts.size(), txt));

        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("INSERT INTO genre (name, code) VALUES (?,?)");
        ) {
            ps.setString(1, name);
            ps.setString(2, code);
            ps.executeUpdate();

        }
    }
}
