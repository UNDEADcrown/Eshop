package by.nosenko.eshop.model;

public class Employee extends Person {
    public Employee(long id, String name, int age) {
        super(id, name, age);
    }
    @Override
    public String toString(){
        return "Employee{" +
                "id" + getId() +
                ", name='" + getName() +
                ", age='" + getAge() +
                '\'' +
                '}';
    }
}
