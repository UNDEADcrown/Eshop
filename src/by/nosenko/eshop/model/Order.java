package by.nosenko.eshop.model;

public class Order {
    private long id; // уникальный номер
    private String customername; //кто покупатель
    private String employeename; //кто продвавец
    private String goods; // список номеров книг, которые продали


    public Order(long id, String customername, String employeename, String goods) {
        this.id = id;
        this.customername = customername;
        this.employeename = employeename;
        this.goods = goods;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getEmployeename() {
        return employeename;
    }

    public void setEmployeename(String employeename) {
        this.employeename = employeename;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }
    @Override
    public String toString(){
        return "Goods{" +
                "id" + getId() +
                ", customername='" + getCustomername() +
                ", employeename='" + getEmployeename() +
                ", goods='" + getGoods() +
                '\'' +
                '}';
    }
}


