package by.nosenko.eshop.model;

public class Goods {
    private long id; //уникальный номер книги
    private String title; //название книги
    private String vendor; //автор книги
    private double price; //цена книги
    private String genre; //жанр книги


    public Goods(long id, String title, String vendor, double price, String genre) {
        this.id = id;
        this.title = title;
        this.vendor = vendor;
        this.price = price;
        this.genre = genre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString(){
        return "Goods{" +
                "id" + getId() +
                ", title='" + getTitle() +
                ", vendor='" + getVendor() +
                ", price='" + getPrice() +
                ", genre='" + getGenre() +
                '\'' +
                '}';
    }
}
