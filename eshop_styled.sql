-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 05 2018 г., 00:08
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `eshop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cooler`
--

CREATE TABLE `cooler` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cooler`
--

INSERT INTO `cooler` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'Thermaltake C1000 Opaque Coolant', 'Thermaltake', 48, 'cooler');

-- --------------------------------------------------------

--
-- Структура таблицы `corp`
--

CREATE TABLE `corp` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `corp`
--

INSERT INTO `corp` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'AeroCool Cylon ', 'AeroCool', 80.8, 'corp');

-- --------------------------------------------------------

--
-- Структура таблицы `cpu`
--

CREATE TABLE `cpu` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cpu`
--

INSERT INTO `cpu` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(2, 'Phenom II', 'AMD', 100, 'cpu'),
(4, 'Athlon 64', 'AMD', 20, 'cpu'),
(6, 'Ryzen', 'AMD', 300, 'cpu'),
(7, 'Phenom II', 'AMD', 100, 'cpu'),
(8, 'Test', 'Vendor', 100, 'cpu');

-- --------------------------------------------------------

--
-- Структура таблицы `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Дамп данных таблицы `customers`
--

INSERT INTO `customers` (`id`, `name`, `age`) VALUES
(1, 'Sidorov Alexey', 32),
(4, 'Simonov Kirill', 32);

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`id`, `name`, `age`) VALUES
(6, 'Petrov Petr', 30),
(9, 'Romanov Dmitrij', 19),
(10, 'Stefan Omber', 35);

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genre`
--

INSERT INTO `genre` (`id`, `name`, `code`) VALUES
(1, 'Cpu', 'cpu'),
(2, 'Videocard', 'videocard'),
(3, 'Motherboard', 'motherboard'),
(5, 'HDD', 'hdd'),
(6, 'Ram', 'ram'),
(7, 'Cooler', 'cooler'),
(8, 'Corp', 'corp'),
(9, 'Powersupply', 'powersupply'),
(10, 'Soundcard', 'soundcard'),
(11, 'Networkadapter', 'networkadapter'),
(12, 'Opticalstorage', 'opticalstorage'),
(13, 'Tvtuner', 'tvtuner'),
(14, 'Mouse', 'mouse'),
(15, 'Keyboard', 'keyboard'),
(16, 'Usbflash', 'usbflash'),
(17, 'Memorycard', 'memorycard');

-- --------------------------------------------------------

--
-- Структура таблицы `hdd`
--

CREATE TABLE `hdd` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `hdd`
--

INSERT INTO `hdd` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(2, 'Barracuda', 'Seagate', 100, 'hdd'),
(3, 'Spinpoint', 'Samsung', 119, 'hdd');

-- --------------------------------------------------------

--
-- Структура таблицы `keyboard`
--

CREATE TABLE `keyboard` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `keyboard`
--

INSERT INTO `keyboard` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'Redragon Varuna ', 'Redragon ', 86.22, 'keyboard');

-- --------------------------------------------------------

--
-- Структура таблицы `memorycard`
--

CREATE TABLE `memorycard` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `memorycard`
--

INSERT INTO `memorycard` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(2, 'Samsung EVO+ microSDXC 64GB  [MB-MC64GA] ', 'Samsung', 46.63, 'memorycard');

-- --------------------------------------------------------

--
-- Структура таблицы `motherboard`
--

CREATE TABLE `motherboard` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `motherboard`
--

INSERT INTO `motherboard` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(2, 'Sybertooth', 'MSI', 150, 'motherboard'),
(3, 'aorus series', 'Gigabyte', 180, 'motherboard'),
(5, 'aorus series', 'Gigabyte', 180, 'motherboard');

-- --------------------------------------------------------

--
-- Структура таблицы `mouse`
--

CREATE TABLE `mouse` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mouse`
--

INSERT INTO `mouse` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'A4Tech Bloody V8M ', 'A4Tech ', 32.13, 'mouse');

-- --------------------------------------------------------

--
-- Структура таблицы `networkadapter`
--

CREATE TABLE `networkadapter` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `networkadapter`
--

INSERT INTO `networkadapter` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'TP-Link UE330 ', 'TP-Link', 44.08, 'networkadapter');

-- --------------------------------------------------------

--
-- Структура таблицы `opticalstorage`
--

CREATE TABLE `opticalstorage` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `opticalstorage`
--

INSERT INTO `opticalstorage` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'ASUS Impresario SBW-S1 PRO ', 'ASUS ', 308.7, 'opticalstorage');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customername` varchar(255) NOT NULL,
  `employeename` varchar(255) NOT NULL,
  `goods` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `customername`, `employeename`, `goods`) VALUES
(1, 'Sidorov Alexey', 'Petrov Petr', 'Ryzen, Titan XP, Sybertooth'),
(2, 'Simonov Kirill', 'Petrov Petr', 'Phenom II, gtx 760m, aorus series'),
(3, 'Sidorov Alexey', 'Petrov Petr', 'Phenom II, GTX 1080TI, Sybertooth'),
(5, 'Simonov Kirill', 'Petrov Petr', 'Athlon 64, GTX 1080TI, Sybertooth'),
(6, 'Sidorov Alexey', 'Romanov Dmitrij', 'Phenom II, GTX 1080TI, Sybertooth, Barracuda'),
(10, 'Sidorov Alexey', 'Petrov Petr', 'Cpu empty, Videocard empty, Motherboard empty, HDD empty, Ram empty, Cooler empty, Corp empty, Powersupply empty, Soundcard empty, Networkadapter empty, Opticalstorage empty, Tvtuner empty, Mouse empty, Keyboard empty, Usbflash empty, Memorycard empty'),
(11, 'Customer empty', 'Employee empty', 'Cpu empty, Videocard empty, Motherboard empty, HDD empty, Ram empty, Cooler empty, Corp empty, Powersupply empty, Soundcard empty, Networkadapter empty, Opticalstorage empty, Tvtuner empty, Mouse empty, Keyboard empty, Usbflash empty, Memorycard empty'),
(12, 'Customer empty', 'Employee empty', 'Cpu empty, Videocard empty, Motherboard empty, HDD empty, Ram empty, Cooler empty, Corp empty, Powersupply empty, Soundcard empty, Networkadapter empty, Opticalstorage empty, Tvtuner empty, Mouse empty, Keyboard empty, Usbflash empty, Memorycard empty'),
(13, 'Sidorov Alexey', 'Romanov Dmitrij', 'Phenom II, GTX 1080TI, Sybertooth, Spinpoint, Ram empty, Cooler empty, Corp empty, Powersupply empty, Soundcard empty, Networkadapter empty, Opticalstorage empty, Tvtuner empty, Mouse empty, Keyboard empty, Usbflash empty, Memorycard empty'),
(15, 'Sidorov Alexey', 'Romanov Dmitrij', 'Phenom II, GTX 1080TI, Sybertooth, Spinpoint, HyperX Fury 8GB DDR4 PC4-19200 HX424C15FB2/8 , Thermaltake C1000 Opaque Coolant, AeroCool Cylon , AeroCool Kcas 700W , ASUS Xonar DG , TP-Link UE330 , ASUS Impresario SBW-S1 PRO , AverMedia Live Gamer HD2 (GC 570) , A4Tech Bloody V8M , Redragon Varuna , Kingston DataTraveler 50 32GB [DT50/32GB] , Samsung EVO+ microSDXC 64GB  [MB-MC64GA] '),
(16, 'Sidorov Alexey', 'Petrov Petr', 'Ryzen, Titan XP, Sybertooth, Spinpoint, HyperX Fury 8GB DDR4 PC4-19200 HX424C15FB2/8 , Thermaltake C1000 Opaque Coolant, AeroCool Cylon , AeroCool Kcas 700W , ASUS Xonar DG , TP-Link UE330 , ASUS Impresario SBW-S1 PRO , AverMedia Live Gamer HD2 (GC 570) , A4Tech Bloody V8M , Redragon Varuna , Kingston DataTraveler 50 32GB [DT50/32GB] , Samsung EVO+ microSDXC 64GB  [MB-MC64GA] '),
(17, 'Sidorov Alexey', 'Stefan Omber', 'Phenom II, GTX 1080TI, Sybertooth, Spinpoint, HyperX Fury 8GB DDR4 PC4-19200 HX424C15FB2/8 , Thermaltake C1000 Opaque Coolant, AeroCool Cylon , AeroCool Kcas 700W , ASUS Xonar DG , TP-Link UE330 , ASUS Impresario SBW-S1 PRO , AverMedia Live Gamer HD2 (GC 570) , A4Tech Bloody V8M , Redragon Varuna , Kingston DataTraveler 50 32GB [DT50/32GB] , Memorycard empty'),
(18, 'Simonov Kirill', 'Petrov Petr', 'Cpu empty, Videocard empty, Motherboard empty, Hdd empty, Ram empty, Cooler empty, Corp empty, Powersupply empty, Soundcard empty, Networkadapter empty, Opticalstorage empty, Tvtuner empty, Mouse empty, Keyboard empty, Usbflash empty, Samsung EVO+ microSDXC 64GB  [MB-MC64GA] ');

-- --------------------------------------------------------

--
-- Структура таблицы `powersupply`
--

CREATE TABLE `powersupply` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `powersupply`
--

INSERT INTO `powersupply` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'AeroCool Kcas 700W ', 'AeroCool ', 105.2, 'powersupply');

-- --------------------------------------------------------

--
-- Структура таблицы `ram`
--

CREATE TABLE `ram` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ram`
--

INSERT INTO `ram` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'HyperX Fury 8GB DDR4 PC4-19200 HX424C15FB2/8 ', 'HyperX', 193.16, 'ram');

-- --------------------------------------------------------

--
-- Структура таблицы `soundcard`
--

CREATE TABLE `soundcard` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `soundcard`
--

INSERT INTO `soundcard` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'ASUS Xonar DG ', 'ASUS ', 68.4, 'soundcard');

-- --------------------------------------------------------

--
-- Структура таблицы `tvtuner`
--

CREATE TABLE `tvtuner` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tvtuner`
--

INSERT INTO `tvtuner` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'AverMedia Live Gamer HD2 (GC 570) ', 'AverMedia', 351.84, 'tvtuner');

-- --------------------------------------------------------

--
-- Структура таблицы `usbflash`
--

CREATE TABLE `usbflash` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `usbflash`
--

INSERT INTO `usbflash` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'Kingston DataTraveler 50 32GB [DT50/32GB] ', 'Kingston', 24.12, 'usbflash');

-- --------------------------------------------------------

--
-- Структура таблицы `videocard`
--

CREATE TABLE `videocard` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `videocard`
--

INSERT INTO `videocard` (`id`, `title`, `vendor`, `price`, `genre`) VALUES
(1, 'GTX 1080TI', 'MSI', 1500, 'videocard'),
(3, 'gtx 760m', 'nvidia', 120, 'videocard'),
(4, 'Titan XP', 'Asus', 1500, 'videocard'),
(5, 'gtx 760m', 'MSI', 100, 'videocard');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cooler`
--
ALTER TABLE `cooler`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `corp`
--
ALTER TABLE `corp`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cpu`
--
ALTER TABLE `cpu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hdd`
--
ALTER TABLE `hdd`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `keyboard`
--
ALTER TABLE `keyboard`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `memorycard`
--
ALTER TABLE `memorycard`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `motherboard`
--
ALTER TABLE `motherboard`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mouse`
--
ALTER TABLE `mouse`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `networkadapter`
--
ALTER TABLE `networkadapter`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `opticalstorage`
--
ALTER TABLE `opticalstorage`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `powersupply`
--
ALTER TABLE `powersupply`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ram`
--
ALTER TABLE `ram`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `soundcard`
--
ALTER TABLE `soundcard`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tvtuner`
--
ALTER TABLE `tvtuner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `usbflash`
--
ALTER TABLE `usbflash`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `videocard`
--
ALTER TABLE `videocard`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cooler`
--
ALTER TABLE `cooler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `corp`
--
ALTER TABLE `corp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `cpu`
--
ALTER TABLE `cpu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `hdd`
--
ALTER TABLE `hdd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `keyboard`
--
ALTER TABLE `keyboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `memorycard`
--
ALTER TABLE `memorycard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `motherboard`
--
ALTER TABLE `motherboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `mouse`
--
ALTER TABLE `mouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `networkadapter`
--
ALTER TABLE `networkadapter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `opticalstorage`
--
ALTER TABLE `opticalstorage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `powersupply`
--
ALTER TABLE `powersupply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `ram`
--
ALTER TABLE `ram`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `soundcard`
--
ALTER TABLE `soundcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `tvtuner`
--
ALTER TABLE `tvtuner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `usbflash`
--
ALTER TABLE `usbflash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `videocard`
--
ALTER TABLE `videocard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
